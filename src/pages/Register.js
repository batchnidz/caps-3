import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import './Register.css'



export default function Register() {
	
	const {user} = useContext(UserContext)
	const navigate = useNavigate()



	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')


	const [isActive, setIsActive] = useState(false)


	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true) {
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Email already exist!'
				})
			} else {
				
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNumber: mobileNumber,
						email:email,
						password: password1
					})
				})
				.then(response => response.json())
				.then(result => {
					console.log(result);
					
					setEmail('');
					setPassword1('');
					setPassword2('');
					setFirstName('');
					setLastName('');
					setMobileNumber('');

					if(result){
						Swal.fire({
							title: 'Register Successful!',
							icon: 'success',
							text: 'Thank you and Enjoy!'
							})
						
						navigate('/login')

					} else {
						Swal.fire({	
							title: 'Registration Failed',
							icon: 'error',
							text: "There is Something wrong! :("
						})
					}		
				})
			}
		})

	}
	

	useEffect(() => {
  if (
    (firstName !== '' &&
      lastName !== '' &&
      mobileNumber.length === 11 &&
      email !== '' &&
      password1 !== '' &&
      password2 !== '') &&
    password1 === password2
  ) {
    setIsActive(true);
  } else {
    setIsActive(false);
  }
}, [firstName, lastName, mobileNumber, email, password1, password2, setIsActive, isActive]);

	return (

		(user.id !== null) ?
			<Navigate to="/"/>
		:
		<>

<div className="container" id="containerONE">
	
	<div className="form-container sign-in-container">
		<form className="reg" onSubmit={event => registerUser(event)}>
			<h1 className="regText">Register</h1>
			<div className="social-container">

				<a href="#containerONE"><img className="sampLOGO" src="https://img.freepik.com/free-photo/vinyl-record-isolated_469584-14.jpg?w=740&t=st=1676739315~exp=1676739915~hmac=1238270c0abb02891abfa774760520fe9abb43a46e0c40a407b491079f173e2a" alt="avatar" width="100" height="100"/></a></div>

			
			<input type="text" placeholder="Firstname" controlid="firstName"  value={firstName} 
				            onChange={event => setFirstName(event.target.value)}
				            required/>
			<input type="text" placeholder="Lastname" controlid="lastName" value={lastName} 
				            onChange={event => setLastName(event.target.value)}
				            required />
			<input type="text" placeholder="Mobile Number" controlid="mobileNumber" value={mobileNumber} 
				            onChange={event => setMobileNumber(event.target.value)}
				            required />
			<input type="text" placeholder="Email" controlid="userEmail" value={email} 
				            onChange={event => setEmail(event.target.value)}
				            required />
			<input type="password" placeholder="Password" controlid="password1" value={password1} onChange={event => setPassword1(event.target.value)}/>
			<input type="password" placeholder="Verify Password" controlid="password2" value={password2} onChange={event => setPassword2(event.target.value)}/>
			<a href="#containerONE">Forgot your password?</a>

			 {	isActive ?
			<button className="butSign" type="submit">Submit</button>
			:
			<button className="butSign" variant="secondary" type="submit" disabled>Submit</button>

			}
		</form>
	</div>
	<div className="overlay-container">
		<div className="overlay">
			<div className="overlay-panel overlay-right">
				<h1>Welcome to<p className="rapganic">Rapganic!</p></h1>
				<p>Shop with us</p>
				
			</div>
		</div>
	</div>
</div>
</>
)
}