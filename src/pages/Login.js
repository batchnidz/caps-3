import {useState, useEffect, useContext} from 'react'
import {Form} from 'react-bootstrap'
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  const retrieveUser = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        setUser({
          id: result._id,
          isAdmin: result.isAdmin,
        });
      });
  };

  const authenticate = (event) => {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (typeof result.access !== 'undefined') {
          localStorage.setItem('token', result.access);
          localStorage.getItem('token', result.access);

          retrieveUser(result.access);


          Swal.fire({
            title: 'Login Successful!',
            icon: 'success',
            text: 'Welcome to Rapganic!',
          });
        } else {
          Swal.fire({
            title: 'Authentication Failed!',
            icon: 'error',
            text: 'Invalid Email or password',
          });
        }
      });
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  if (user.id !== null) {
    if (user.isAdmin) {
      return <Navigate to="/Admin" />;
    } else {
      return <Navigate to="/products" />;
    }
  }

  return (
    <>
      <div className="container" id="containerONE">
        <div className="form-container sign-in-container">
          <Form className="reg" onSubmit={event =>authenticate(event)}>
            <h1 className="logText">Login</h1>
            <div className="social-container">
              <a href="#containerONE">
                <img
                  src="https://img.freepik.com/free-photo/vinyl-record-isolated_469584-14.jpg?w=740&t=st=1676739315~exp=1676739915~hmac=1238270c0abb02891abfa774760520fe9abb43a46e0c40a407b491079f173e2a"
                  alt="avatar"
                  width="100"
                  height="100"
                />
              </a>
            </div>
            <input
              type="text"
              placeholder="Email"
              controlid="userEmail"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              required
            />
            <input
              type="password"
              placeholder="Password"
              controlid="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
            <a href="#containerONE">Forgot your password?</a>

            {isActive ? (
              <button className="butSign" type="submit">
                Submit
              </button>
            ) : (
              <button
                className="butSign"
                variant="secondary"
                type="submit"
                disabled
              >
                Submit
              </button>
            )}
          </Form>
        </div>
        <div className="overlay-container">
          <div className="overlay">
            <div className="overlay-panel overlay-right">
              <h1>WELCOME BACK!</h1>
				<div>Don't panic shop at <p className="rapganic"> Rapganic</p></div>
				
			</div>
		</div>
	</div>
</div>
</>
)
}
