import ProductCard from '../components/ProductCard'
import Loading from '../components/Loading'
import {useEffect, useState} from 'react'

export default function Products(){
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(true)

	useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/`)
      .then(response => response.json())
      .then(result => {
        console.log(result)
        setProducts(
          result.filter(product => product.isActive)
                .map(product => <ProductCard key={product._id} productProp={{...product, isActive: true}} />)
        )
        setIsLoading(false)
      })
  }, [])




	return(
			
				(isLoading) ?
					<Loading/>
			:
				<>
					{products}
				</>
	)
}
