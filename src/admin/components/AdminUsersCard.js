import {Card, Button} from 'react-bootstrap'
import UserContext from '../../UserContext'
import { useContext } from 'react'
import Swal from 'sweetalert2';


export default function AdminUsersCard(users) {
  
  const { user } = useContext(UserContext);




  const { firstName, lastName, mobileNumber, email, isAdmin, _id } = users.userProp ;




 const makeadmin = (event) => {
    if (event) {
      event.preventDefault();
    }

    if (user.isAdmin) {
      const makeAdmin = {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          isAdmin: false,
        }),
      };

      fetch(`${process.env.REACT_APP_API_URL}/users/makeAdmin/${_id}`, makeAdmin)
        .then((res) => res.json())
        .then((data) => {
          console.log('status updated', data);
          Swal.fire('Success', 'User Promoted successfully', 'success');
          setTimeout(() => {
          window.location.reload();
        }, 1000);
        })
        .catch((error) => {
        console.error('Error Promoting User:', error);
        Swal.fire('Error', 'Unable to Prmote User', 'error');
        });
    }
  };



  const demote = (event) => {
  if (event) {
    event.preventDefault();
  }

  if (user.isAdmin) {
    const demoteUser = {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        isAdmin: true,
      }),
    };

    fetch(`${process.env.REACT_APP_API_URL}/users/demoteAdmin/${_id}`, demoteUser)
      .then((res) => res.json())
      .then((data) => {
         console.log('status updated', data);
          Swal.fire('Success', 'Admin Demoted successfully', 'success');
          setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
       .catch((error) => {
        console.error('Error Demoting Admin:', error);
        Swal.fire('Error', 'Unable to Demote Admin', 'error');
        });
  }

}



  return (
    <>

     

   
        <Card className="col-3 product-card">
          <Card.Body className="AProd">
            <Card.Title>User</Card.Title>
            <hr className="hori" />
            <Card.Subtitle className="nameUsers">{firstName || 'N/A'} {lastName || 'N/A'}</Card.Subtitle>
            <Card.Subtitle className="mobNum">{mobileNumber || 'N/A'}</Card.Subtitle>
            <Card.Text className="emailUser">{email || 'N/A'}</Card.Text>
            <Card.Text>{isAdmin.toString() || 'N/A'}</Card.Text>
           
               {isAdmin ? 
        <Button className="btn btn-success" id="btnDet4" onClick={demote}>
          Demote
        </Button>
        :
        <Button className="btn btn-success" id="btnDet4" onClick={makeadmin}>
         Make Admin
        </Button>
      }

          
            
             
          </Card.Body>
        </Card>

    </>
  );
}