import { useState, useEffect, useContext } from 'react';
import { Button } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../../UserContext'
import Swal from 'sweetalert2'


export default function AdminProductView(product) {
  // Gets the productId from the URL of the route that this component is connected to. '/products/:productId'
  const { productId } = useParams();

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState('');
  const [quantity, setquantity] = useState('')



  	const checkout = (productId) => {
  if (user.isAdmin) {
    Swal.fire({
      title: 'Error!',
      icon: 'error',
      text: 'Admin users are not allowed to order.',
    });
    return;
  }



    fetch(`${process.env.REACT_APP_API_URL}/users/createorders`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        userId: user.id,
        quantity: quantity,
        price: price * quantity,
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Failed to create order');
        }
        return response.json();
      })
      .then((result) => {
        if (result) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'You have ordered successfully!',
          });

          navigate('/AdminProducts');
        } else {
          console.log(result);

          Swal.fire({
            title: 'Something went wrong!',
            icon: 'error',
            text: 'Please try again :(',
          });
        }
      })
      .catch((error) => {
        console.error(error);

        Swal.fire({
          title: 'Something went wrong!',
          icon: 'error',
          text: 'Please try again :(',
        });
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((result) => {
        console.log(result.name);
        console.log(result.price);
        console.log(result.image);
        setName(result.name);
        setDescription(result.description);
        setPrice(result.price);
        setquantity(result.quantity)
        setImage(result.image);
      });
  }, [productId]);




	return(
	<>
		
		<div class="container">
    <div class="card">
        <div class="card-body">
            <h3 class="card-title">{name}</h3>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-6">
                    <div class="white-box text-center"><img src={`${process.env.REACT_APP_API_URL}/uploads/${image}`} alt="" className="img-responsive cole"/></div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-6">
                    <h4 class="box-title mt-5">Product description</h4>
                    <p>{description}</p>
                    <b><p>Stocks</p></b>
                      <p> {quantity}</p>
                    <h2 class="mt-5 php">
                        PHP {price.toLocaleString()}
                    </h2>
                   
              
                   <div className="ViewDes"><i class="fa-solid fa-coins coinss"></i><p className="moneyT">MONEY BACK GUARANTEED</p></div>
                   <div className="ViewText"><i class="fa-solid fa-truck-fast"></i><p className="moneyT"> SHIPPING 7 DAYS</p></div>
                <div className="ViewText"><i class="fa-solid fa-hand-holding-dollar"></i><p className="moneyT"> CASH ON DELIVERY</p></div>

                </div>

                 
                </div>

               
<div className="buttonsV">
 

                   


              { user.id !== null ? 
                  <Button variant="success" id="btnView" onClick={() => checkout(productId)}>Checkout</Button>
                :
                  <Link className="btn btn-danger btn-block" id="btnViewlog" to="/login">Log in to Order</Link>
              }

              <button class="btn btn-dark btn-rounded mr-1" data-toggle="tooltip" title="" data-original-title="Add to cart">
                        <i class="fa fa-shopping-cart"></i>
                    </button>
                  </div>
                </div>
                  </div>
                    </div>
	</>
	)
}