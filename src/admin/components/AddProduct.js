import { Modal, Button, Form } from 'react-bootstrap';
import React from 'react'
import { useState } from 'react'
import Swal from 'sweetalert2';





function AddProducts(){

   const token = localStorage.getItem('token');
 
 


 const [showModal, setShowModal] = useState(false);
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productPrice, setProductPrice] = useState(0);
  const [productQuantity, setProductQuantity] = useState(0);


  const [fileName, setFileName] = useState('')



  const onChangeFile = (e) =>{
    e.preventDefault();
    setFileName(e.target.files[0]);
  }



const ToAddProduct = async (token) => {
 

  const formData = new FormData();
  formData.append('name', productName);
  formData.append('description', productDescription);
  formData.append('price', productPrice);
  formData.append('quantity', productQuantity);
  formData.append('image', fileName);

const options = {
  method: 'POST',
  body: formData,
  headers:{
    "Authorization": `Bearer ${localStorage.getItem("token")}`
  },
};

try{
  const response = await fetch(`${process.env.REACT_APP_API_URL}/products/addProduct/`, options);
  const data = await response.json();
  console.log(data);
  setShowModal(false);
    Swal.fire({
      icon: 'success',
      title: 'Product added successfully',
      showConfirmButton: false,
      timer: 1500
    })
} catch (error) {
  console.error(error);
  Swal.fire({
    icon: 'error',
    title: 'Error',
    text: 'Unable to add product. Please try again later.',
    confirmButtonText: 'OK'
  });
}




}

 return(
  <>
    <Button className="ml-auto" variant="success" id="btnDet2" onClick={() => setShowModal(true)}>Add Product</Button>




      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form encType="multipart/form-data">
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control type="text" placeholder="Enter name" value={productName} onChange={(e) => setProductName(e.target.value)} />
            </Form.Group>

            <Form.Group controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control type="text" placeholder="Enter description" value={productDescription} onChange={(e) => setProductDescription(e.target.value)} />
            </Form.Group>

            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control type="text" placeholder="Enter Amount" value={productPrice} onChange={(e) => setProductPrice(e.target.value)} />
            </Form.Group>

            <Form.Group controlId="productQuantity">
              <Form.Label>Quantity</Form.Label>
              <Form.Control type="number" placeholder="Enter quantity" value={productQuantity} onChange={(e) => setProductQuantity(e.target.value)} />
            </Form.Group>

           <Form.Group controlId="fileName">
  <Form.Label>Choose File</Form.Label>
  <Form.Control type="file" filename="image" onChange={onChangeFile} />
</Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowModal(false)}>Cancel</Button>
          <Button variant="success" type="Submit" className="btnADD" onClick={() => ToAddProduct(token)}>Add Product</Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
  

export default AddProducts;










