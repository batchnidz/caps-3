import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { Modal, Button, Form } from 'react-bootstrap';
import {useState} from 'react';
import UserContext from '../../UserContext'
import { useContext } from 'react'
import Swal from 'sweetalert2';



export default function AdminProductCard(product) {





   const { user } = useContext(UserContext);

  const { name, description, price, quantity, image, _id, isActive } = product.productProp;

  const [showEditModal, setShowEditModal] = useState(false);
  const [updatedProduct, setUpdatedProduct] = useState({
    name: name,
    description: description,
    price: price,
    quantity: quantity,
    _id: _id,
    isActive: isActive
  });








  const Edit = () => {
    setShowEditModal(true);
  };

  const Exit = () => {
    setShowEditModal(false);
  };

  const Update = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    setUpdatedProduct((prevProduct) => {
      return {
        ...prevProduct,
        [name]: value
      };
    });
  };

const Submit = (event) => {
  if (event) {
    event.preventDefault();
  }



  if (user.isAdmin) {
    const { _id } = updatedProduct;
    const updateProduct = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(updatedProduct),
    };
    fetch(`${process.env.REACT_APP_API_URL}/products/updatedProduct/${_id}`, updateProduct)
      .then((response) => response.json())
      .then((data) => {
        console.log('Product updated', data);
        Exit();
        Swal.fire('Success', 'Product updated successfully', 'success');
      })
      .catch((error) => {
        console.error('Error updating product:', error);
        Swal.fire('Error', 'Unable to update product', 'error');
      });
  }
};

 const archive = (event) => {
    if (event) {
      event.preventDefault();
    }

    if (user.isAdmin) {
      const archiveProduct = {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          isActive: false,
        }),
      };

      fetch(`${process.env.REACT_APP_API_URL}/products/archiveProduct/${_id}`, archiveProduct)
        .then((res) => res.json())
        .then((data) => {
          console.log('status updated', data);
          Swal.fire('Success', 'Product Archived successfully', 'success');
          setTimeout(() => {
          window.location.reload();
        }, 1000);
        })
        .catch((error) => {
        console.error('Error Archiving product:', error);
        Swal.fire('Error', 'Unable to Archived product', 'error');
        });
    }
  };



const unarchive = (event) => {
  if (event) {
    event.preventDefault();
  }

  if (user.isAdmin) {
    const unarchiveProduct = {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        isActive: true,
      }),
    };

    fetch(`${process.env.REACT_APP_API_URL}/products/unarchivedProduct/${_id}`, unarchiveProduct)
      .then((res) => res.json())
      .then((data) => {
         console.log('status updated', data);
          Swal.fire('Success', 'Product Unarchived successfully', 'success');
          setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
       .catch((error) => {
        console.error('Error Unarchiving product:', error);
        Swal.fire('Error', 'Unable to Unarchived product', 'error');
        });
  }

}



  return (
    <>
      
      <Card className="col-3 product-card">
        <Card.Body className="AProd" >
          <Card.Title className="productNames">{name}</Card.Title>
          <hr className="hori"/>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PHP {price.toLocaleString()}</Card.Text>
          <Card.Text>Stocks</Card.Text>
          <Card.Text>{quantity}</Card.Text>
     
   <div> <img src={`${process.env.REACT_APP_API_URL}/uploads/${image}`} alt="" className="imgg"/> </div>
        
          <Link className="btn btn-success" id="btnDet" to={`/AdminProductView/${_id}`}>
            Order
          </Link>

          <Button className="btn btn-success" id="btnDet3" onClick={Edit}>
            Update
          </Button>

          {isActive ? 
        <Button className="btn btn-success" id="btnDet4" onClick={archive}>
          Archive
        </Button>
        :
        <Button className="btn btn-success" id="btnDet4" onClick={unarchive}>
          Unarchive
        </Button>
      }
        </Card.Body>
      </Card>


      <Modal show={showEditModal} onHide={Exit}>
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={Submit}>
            <Form.Group controlId="formProductName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={updatedProduct.name}
                onChange={Update}
              />
            </Form.Group>
            <Form.Group controlId="formProductDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                name="description"
                value={updatedProduct.description}
                onChange={Update}
              />
            </Form.Group>
            <Form.Group controlId="formProductPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                name="price"
                value={updatedProduct.price}
                onChange={Update}
              />
            </Form.Group>
            <Form.Group controlId="formProductQuantity">
              <Form.Label>Quantity</Form.Label>
              <Form.Control
                type="number"
                name="quantity"
                value={updatedProduct.quantity}
                onChange={Update}
              />
            </Form.Group>
           {/* <Form.Group controlId="formProductImage">
              
          <Form.Label>Image URL</Form.Label>
          <Form.Control
            type="text"
            name="image"
            value={updatedProduct.image}
            onChange={Update}
          />
        </Form.Group>*/}
        <Button variant="secondary" onClick={Exit}>
          Cancel
        </Button>
        <Button variant="success" type="submit" className="btnDet5" onClick={() => Submit()}>
          Save Changes
        </Button>
      </Form>
    </Modal.Body>
  </Modal>
</>

);
}