import AdminProductCard from '../components/AdminProductCard'
import Loading from '../components/AdminLoading'
import AddProducts from '../components/AddProduct'


import {useEffect, useState} from 'react'

export default function AdminProducts(){
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(true)

	useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/`)
      .then(response => response.json())
      .then(result => {
        console.log(result)
        setProducts(
          result.map(product => {
               return ( 
               	<AdminProductCard key={product._id} productProp={product} />
               	)
          })
          )
        
        setIsLoading(false)
      })
  }, [])




	return(
			
				(isLoading) ?
					<Loading/>
			:
				<>
				<AddProducts />
				<div className="product-grid">
					{products}
					</div>
				</>
	)
}
