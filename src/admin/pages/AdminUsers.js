import AdminUsersCard from '../components/AdminUsersCard'
import Loading from '../components/AdminLoading'

import {useEffect, useState} from 'react'

export default function AdminUsers(){
  const [users, setUsers] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/getAllUsers`)
      .then(response => response.json())
      .then(result => {
        console.log(result)
        setUsers(
          result.map(user => {
               return ( 
                <AdminUsersCard key={user._id} userProp={user} />
                )
          })
          )
        
        setIsLoading(false)
      })
  }, [])




  return(
      
        (isLoading) ?
          <Loading/>
      :
        <>
   
        <div className="product-grid">
          {users}
          </div>
        </>
  )
}
