import './App.css';
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import {Container} from 'react-bootstrap'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Logout from './pages/Logout'
import PageNotFound from './pages/ErrorPage'
import ProductView from './components/ProductView'
import AdminHome from './admin/AdminHome'
import AdminProducts from './admin/pages/AdminProducts'
import AdminProductView from './admin/components/AdminProductView'
import AdminUsers from './admin/pages/AdminUsers'

import { useState, useEffect } from 'react'

import { UserProvider } from './UserContext'


// Initialize the dynamic routing will be involved

// Common patterns in react.js for a component to return multilple elements.


function App() {
  const [user, setUser] = useState({
  id: null,
  isAdmin: null,
});

const unsetUser = () => {
  localStorage.removeItem('token');
  setUser({
    id: null,
    isAdmin: null,
  });
};

useEffect(() => {
  const token = localStorage.getItem('token');
  if (token) {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        if (res.status === 401) {
          unsetUser();
        } else {
          return res.json();
        }
      })
      .then((data) => {
        if (data) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        }
      })
      .catch((error) => {
        console.log('Error fetching user details:', error);
        unsetUser();
      });
  }
}, []);



  return (
    <>


      {/*Provides the user context throughout any component inside of it.*/}
      <UserProvider value={{user, setUser, unsetUser}}>
        {/*Initializes that dynamic routing will be involved*/}
        <Router>
          <AppNavbar/>
             
          <Container>
            <Routes>
            <Route path="/Admin" element={<AdminHome />}/>
            <Route path="/AdminProducts" element={<AdminProducts/>}/>
             <Route path="/AdminProductView/:productId" element={<AdminProductView/>}/>
              <Route path="/" element={<Home/>}/>
              <Route path="/products" element={<Products/>}/>
              <Route path="/AdminUsers" element={<AdminUsers />}/>
              <Route path="/products/:productId" element={<ProductView/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>} />
              <Route path="/*" element={<PageNotFound/>}/>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;

