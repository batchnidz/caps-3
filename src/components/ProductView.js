import { useState, useEffect, useContext } from 'react';
import { Card, Button } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function ProductView(product) {
  // Gets the productId from the URL of the route that this component is connected to. '/products/:productId'
  const { productId } = useParams();

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState('');
  const [Quantity, setQuantity] = useState(1);
  const [quantity, setquantity] = useState('')

  const checkout = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/createorders`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        userId: user.id,
        quantity: Quantity,
        price: price * setQuantity,
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Failed to create order');
        }
        return response.json();
      })
      .then((result) => {
        if (result) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'You have ordered successfully!',
          });

          navigate('/products');
        } else {
          console.log(result);

          Swal.fire({
            title: 'Something went wrong!',
            icon: 'error',
            text: 'Please try again :(',
          });
        }
      })
      .catch((error) => {
        console.error(error);

        Swal.fire({
          title: 'Something went wrong!',
          icon: 'error',
          text: 'Please try again :(',
        });
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((result) => {
        console.log(result.name);
        console.log(result.price);
        console.log(result.image);
        setName(result.name);
        setDescription(result.description);
        setPrice(result.price);
        setquantity(result.quantity)
        setImage(result.image);
      });
  }, [productId]);

   const QuantityChange = (event) => {
    setQuantity(event.target.value);
     const newQuantity = event.target.value;
  if (newQuantity < 1) {
    setQuantity(1);
  } else {
    setQuantity(newQuantity);
  }
  };

  const calculateTotal = () => {
    return price * Quantity;
  };


	 return (
    <>
      <div className="container">
        <div className="card">
          <div className="card-body">
            <h3 className="card-title">{name}</h3>
            <div className="row">
              <div className="col-lg-5 col-md-5 col-sm-6">
                <div className="white-box text-center">
                  <img src={`${process.env.REACT_APP_API_URL}/uploads/${image}`} alt="" className="img-responsive cole" />
                </div>
              </div>
              <div className="col-lg-7 col-md-7 col-sm-6">
                <h4 className="box-title mt-5">Product description</h4>
                <p>{description}</p>
                <b>
                  <p>Stocks</p>
                </b>
                <p> {quantity}</p>
                <h2 className="mt-5 php">PHP {calculateTotal().toLocaleString()}</h2>
                <Card.Subtitle>Quantity:</Card.Subtitle>
                <input type="number" className="qtyView" value={Quantity} onChange={QuantityChange} />

                <div className="ViewDes">
                  <i className="fa-solid fa-coins coinss"></i>
                  <p className="moneyT">MONEY BACK GUARANTEED</p>
                </div>
                <div className="ViewText">
                  <i className="fa-solid fa-truck-fast"></i>
                  <p className="moneyT"> SHIPPING 7 DAYS</p>
                </div>
                <div className="ViewText">
                  <i className="fa-solid fa-hand-holding-dollar"></i>
                  <p className="moneyT"> CASH ON DELIVERY</p>
                </div>
              </div>
            </div>

            <div className="buttonsV">
              {user.id !== null ? (
                <Button variant="success" id="btnView" onClick={() => checkout(productId)}>
                  Checkout
                </Button>
              ) : (
                <Link className="btn btn-danger btn-block" id="btnViewlog" to="/login">
                  Log in to Order
                </Link>
              )}

              <button className="btn btn-dark btn-rounded mr-1" data-toggle="tooltip" title="" data-original-title="Add to cart">
                <i className="fa fa-shopping-cart"></i>
              </button>
            </div>
          </div>
        </div>
      </div>

              
{/*
		<Container className="mt-5">
			<Row className="viewprodRow">
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className="viewprodCon">
						<Card.Body className="text-center term viewprod">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							{/*<CardImg top src={price}/>*/}
						{/*	<Card.Text>PhP {price}</Card.Text>
               <Card.Text>Stock</Card.Text>
              <Card.Text>{quantity}</Card.Text>
							 <Card.Subtitle>Quantity:</Card.Subtitle>
                <input type="number" value={Quantity} onChange={QuantityChange} />
							<CardImg top src={image}/>*/}


							{ /*	user.id !== null ? 
									<Button variant="success" id="btnView" onClick={() => checkout(productId)}>Checkout</Button>
								:
									<Link className="btn btn-danger btn-block" id="btnViewlog" to="/login">Log in to Order</Link>
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container> */}
	</>
	)
}