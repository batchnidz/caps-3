import {Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function ProductCard(product){

 
   const {name, description, price, quantity, image, _id, isActive} = product.productProp

 
 
  if (!isActive) {
    return null
  }
  
  return(
    <>
    <div className="conProd">
    <h1 className="ProdT">{name}</h1>


<img className="imahe" src={`${process.env.REACT_APP_API_URL}/uploads/${image}`} alt="" />
<Container className="PC">
    <div className="dictionary">
    <div className="term">
    <dt>
      <span>{name}</span>
      <hr className="hori"/>
    </dt>
    <div className="dd">
    <div className="desT"> {description} </div>
      <div className="table">
        <div className="thead">
          <div className="tr">
          <div className="Prce">PHP {price.toLocaleString()}</div>
         

           
          </div>
          <div className="tr">
          <div className="th">Stocks</div>
         
           
           
          </div>
            <div className="tr">
          <div className="th">{quantity}</div>
         
           
           
          </div>
        </div>
        <div className="tbody">
          <div className="tr">
            <Link className="btn btn-success" id="btnDet" to={`/products/${_id}`}>Order</Link>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
</Container>
</div>
    </>
  )
}
