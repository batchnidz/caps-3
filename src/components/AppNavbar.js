import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink } from 'react-router-dom'
import {useContext} from 'react'
import UserContext from '../UserContext'



export default function AppNavbar() {
 const { user } = useContext(UserContext);
 console.log(user)



 return (

  <Navbar bg="transparent" expand="lg">
  <div className="container-fluid transpo">
    <Navbar.Brand as={Link} to="/">
      Rapganic
    </Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="ml-auto">
        {user.isAdmin ? (
          <>
            <Nav.Link className="textAPP" as={NavLink} to="/Admin">
              Home
            </Nav.Link>
            <Nav.Link className="textAPP" as={NavLink} to="/AdminProducts">
              Products
            </Nav.Link>
              <Nav.Link className="textAPP" as={NavLink} to="/AdminUsers">
              Users
            </Nav.Link>
            <Nav.Link className="textAPP" as={NavLink} to="/logout">
              Logout
            </Nav.Link>
          </>
        ) : (
          <>
            <Nav.Link className="textAPP" as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link className="textAPP" as={NavLink} to="/products">
              Products
            </Nav.Link>
            {user.id ? (
              <Nav.Link className="textAPP" as={NavLink} to="/logout">
                Logout
              </Nav.Link>
            ) : (
              <>
                <Nav.Link className="textAPP" as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link className="textAPP" as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </>
        )}
      </Nav>
    </Navbar.Collapse>
    </div>
  </Navbar>
);

}