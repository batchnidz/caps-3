import React, { useState, useEffect } from 'react';
import '../App.css'
import { Carousel as ResponsiveCarousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';




const images = [
  'https://akns-images.eonline.com/eol_images/Entire_Site/2020517/rs_1024x759-200617065905-1024-J-Cole-LT-061720-GettyImages-1206745834.jpg?fit=around%7C1024:759&output-quality=90&crop=1024:759;center,top',
  'https://phantom-marca.unidadeditorial.es/465baf437e83c0a2578c589d9dbfd667/resize/660/f/webp/assets/multimedia/imagenes/2022/10/19/16661328920205.jpg',
  'https://dvfnvgxhycwzf.cloudfront.net/media/SharedImage/image600/.fpioDFhX/SharedImage-116679.png?t=58493e3144344aaaae98'
];

const slideDelay = 3000; 

function MyCarousel() {

  const [currentImage, setCurrentImage] = useState(0);

   const handleSlideChange = index => {
    setCurrentImage(index);
}
  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentImage((currentImage + 1) % images.length);
    }, slideDelay);
    return () => clearInterval(intervalId);
  }, [currentImage]);

  return (

    <>
    <div className="CaroCon">
    <ResponsiveCarousel className="carousel-image"
      selectedItem={currentImage}
      onChange={handleSlideChange}  showArrows={false} showThumbs={false} autoPlay={true} interval={slideDelay}>
      {images.map((imageUrl, index) => (
        <div key={imageUrl}>
          <img
            src={imageUrl}
            alt={`Slide ${index + 1}`}
            className="carousel-image"
          />
        </div>
      ))}
    </ResponsiveCarousel>


   <div className="col-lg-8 order-lg-1 mb-5 mb-lg-0 sideB">
                        <div className="container-fluid px-5">
                            <div className="row gx-5">
                                <div className="col-md-6 mb-5">
                                  
                                    <div className="text-center">
                                        <i className="fas fa-fire"></i>
                                        <h3 className="font-alt">HOT ARTIST</h3>
                                        <p className="mb-0 hotT">Music on Spotify is on Top 100, Almost 700k sales in a year!</p>
                                    </div>
                                </div>
                                <div className="col-md-6 mb-5">
                                  
                                    <div className="text-center">
                                        <i className="fa-solid fa-heart"></i>
                                        <h3 className="font-alt">FAVORITE ARTIST</h3>
                                        <p className="mb-0 favT">The ARTIST with most FANS around the Globe, Almost 500k sales in a year!</p>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6 mb-5 mb-md-0">
                                
                                    <div className="text-center">
                                        <i className="fa-solid fa-heart-crack"></i>
                                        <h3 className="font-alt">HATED ARTIST</h3>
                                        <p className="mb-0 hateT">Hate Comments & Hate Speech, Yet earns him 400k sales a year!</p>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                 
                                    <div className="text-center">
                                       <i className="fa-solid fa-gem"></i>
                                        <h3 className="font-alt">UNDERGROUND ARTIST</h3>
                                        <p className="mb-0 undT">Music with passion, Earns him 1k sales in total!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                          </div>





                          <footer className="footer">
                          <div className="container" id="containerID">
                          <div className="row">
                          <div className="col-md-5">
                          <h5>RAPGANIC</h5>
                          <div className="row">
                          <div className="col-6">
                          <ul className="list-unstyled">
                          <li><a href="#containerID">Products</a></li>
                          <li><a href="#containerID">Collaborations</a></li>
                          <li><a href="#containerID">Areas</a></li>
                          <li><a href="#containerID">Team</a></li>
                          </ul>
                          </div>
                          <div className="col-6">
                          <ul className="list-unstyled">
                          <li><a href="#containerID">Support</a></li>
                          <li><a href="#containerID">Legal Terms</a></li>
                          <li><a href="#containerID">About</a></li>
                          </ul>
                          </div>
                          </div>
                          <ul className="nav">
                          <h6 className="fol">Follow us</h6>
                          <li className="nav-item"><a href="#containerID" className="nav-link pl-0"><i className="fa-brands fa-facebook"></i></a></li>
                          <li className="nav-item"><a href="#containerID" className="nav-link"><i className="fa-brands fa-twitter"></i></a></li>
                          <li className="nav-item"><a href="#containerID" className="nav-link"><i className="fa-brands fa-github"></i></a></li>
                          <li className="nav-item"><a href="#containerID" className="nav-link"><i className="fa-brands fa-instagram"></i></a></li>
                          </ul>
                          <br/>
                          </div>
                          <div className="col-md-2">
                          <h5 className="text-md-right">In connection with</h5>
                          <hr/>
                          </div>
                          <div className="col-md-5">
                          <form className="delibs">
                          <img className="jnt" src="https://mir-s3-cdn-cf.behance.net/project_modules/fs/2ff7e472085429.5be3b966651f3.png" alt=""/>
                           <img className="jnt" src="https://www.dafont.com/forum/attach/orig/1/0/1010669.png?1" alt=""/>
                               <img className="jnt" src="https://logos-world.net/wp-content/uploads/2020/11/Eminem-Logo.png" alt=""/>
                                <img className="jnt" src="https://www.seekpng.com/png/full/172-1724437_j-cole-j-cole-font.png" alt=""/>
                          </form>
                          </div>
                          </div>
                          </div>
                          <div className="footC">&copy; Rapganic 2023 </div>
                          </footer>




                            </>

  );
}

export default MyCarousel;
